<div class="wrap">
    <h2>RAA: блоки</h2>

    <p><b>Example:</b></p>
    <p>[raaBlock token="utipo" url="http://yandex.ru"]Отсюда мы пойдем на {a}Яндекс{/a}.[/raaBlock]</p>

    <form method="post" action="options.php">
		<?php wp_nonce_field( 'update-options' ); ?>
		<?php settings_fields( 'raa-blocks' ); ?>

        <table class="form-table">

            <tr valign="top">
                <th scope="row">Показывать блоки:</th>
                <td><input type="checkbox" name="raa-blocks[is_visible]"
                           value="1" <?= ! empty( RaaBlocks::$options['is_visible'] ) ? 'checked' : ''; ?>/></td>
            </tr>

        </table>

        <input type="hidden" name="action" value="update"/>

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>"/>
        </p>

    </form>
</div>
