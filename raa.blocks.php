<?php
/*
Plugin Name: RAA.Blocks
Description: Add shortcode blocks to fast control to show/hide
Version: 0.1
License: GPL2

*/

/**
 * Class RaaBlocks
 *
 * Add blocks
 * Example: [raaBlock token="utipo" url="http://yandex.ru"]Отсюда мы пойдем на {a}Яндекс{/a}.[/raaBlock]
 */
class RaaBlocks {
	public static $options = [
		'is_visible' => '0',
	];
	static $_styleIncluded = false;

	/**
	 * Block
	 *
	 * @param array $attr
	 *
	 * @return string
	 */
	public static function shortcodeBlock( $attr = [], $content = "" ) {
		$attrs = shortcode_atts(
			[
				'token'  => '',
				'url'    => '',
				'style'  => '',
				'target' => '_blank'
			], $attr
		);

		self::getOptions();

		//var_dump( self::$options, $attrs, $content );

		if ( empty( $attrs['token'] ) || empty( self::$options['is_visible'] ) ) {
			return '';
		}

		//ga('send', 'event', 'category', 'action', 'label', value);  // value is a number.
		$toreplace = [
			'{a}'  => '<a href="{url}" target="{target}" class="raa-counter-click" data-action="block" data-category="{token}" data-label="click">',
			'{/a}' => '</a>',
		];
		array_walk(
			$attrs,
			function ( &$item, $k ) use ( &$toreplace ) {
				$toreplace[ '{' . $k . '}' ] = $item;
			}
		);

		// replace first time
		$content = strtr( $content, $toreplace );
		// replace second time
		$content = strtr( $content, $toreplace );

		$html = '<div class="raa-block">' . $content . '</div>';

		// Add styles
		if ( ! self::$_styleIncluded ) {
			add_action( 'wp_footer', [ 'RaaBlocks', 'footer' ] );
			self::$_styleIncluded = true;
		}

		return $html;

	}

	protected static function getOptions() {
		$options = get_option( 'raa-blocks' );
		if ( ! empty( $options ) ) {
			self::$options = $options;
		}

	}

	public static function footer() {
		?>
        <style rel="stylesheet" type="text/css">
            .raa-block { padding: 10px 0; }
        </style>
		<?php
	}

	public static function activate() {
		add_option( 'raa-blocks', self::$options );
	}

	public static function deactive() {
		delete_option( 'raa-blocks' );
	}

	public static function adminInit() {
		register_setting( 'raa-blocks', 'raa-blocks' );

	}

	public static function adminMenu() {
		add_options_page(
			'RAA.Blocks', 'RAA.Blocks',
			'manage_options', 'RaaBlocks',
			[ 'RaaBlocks', 'optionsPage' ]
		);

	}

	public static function optionsPage() {
		self::getOptions();
		include dirname( __FILE__ ) . '/options.php';

	}

}

register_activation_hook( __FILE__, [ 'RaaBlocks', 'activate' ] );
register_deactivation_hook( __FILE__, [ 'RaaBlocks', 'deactive' ] );

if ( is_admin() ) {
	add_action( 'admin_init', [ 'RaaBlocks', 'adminInit' ] );
	add_action( 'admin_menu', [ 'RaaBlocks', 'adminMenu' ] );
} else {
	add_shortcode( 'raaBlock', [ 'RaaBlocks', 'shortcodeBlock' ] );
}